from libs import *
import csv
import os


dns = [
    '10.17.36.10:53',
    '10.17.36.11:53'
]

with open(os.path.join('trash', '1.txt')) as file:
    lines = csv.DictReader(file)
    ips = [i['ip'] for i in lines]

for ip in ips:
    for d in dns:
        print(f'{ip} -> {d}')
