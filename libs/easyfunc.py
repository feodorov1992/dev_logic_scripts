def progress_line(curr_n, max_n):
    progress = 100 * curr_n // max_n
    s = '|{:.<50}|{:>3}% ({}/{})'.format("▮" * (progress // 2), progress, curr_n, max_n)
    print(f'\r{s}', end='')
    if curr_n == max_n:
        print()
