#!./env/bin/python
import re
import paramiko
import subprocess
import argparse
import csv
from libs.tslogging import TsLogging
from libs.easyfunc import progress_line
from pythonping import ping
from getpass import getpass


def ip_filter(ip_addr_list: list, exclude: list = None):
    if exclude is None:
        exclude = ['127', '192']
    for ip in ip_addr_list:
        pref = ip.split('.')[0]
        if not pref in exclude:
            return ip
    else:
        return ip_addr_list


def isping(addr: str) -> bool:
    resp = ping(
        addr,
        count=1,
        timeout=1
    )
    return str(resp).startswith('Reply')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script is used ...')
    parser.add_argument('--dry-run', action='store_true', help='dry run, do not process')
    TsLogging.add_args(parser)
    args = parser.parse_args()
    log = TsLogging(args)

    ip_regex = r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'

    ip_entries_raw = subprocess.run(['ip', 'addr'], encoding='utf-8', stdout=subprocess.PIPE).stdout
    ip_entries = re.findall(ip_regex, ip_entries_raw)

    first, second, _, _ = ip_filter(ip_entries).split('.')

    ips = list()
    max_ind = 256
    print('Collecting live ip addresses...')
    for third in range(max_ind):
        progress_line(third, max_ind - 1)
        if isping(f'{first}.{second}.{third}.1'):
            network = f'{first}.{second}.{third}.0/24'
            stdout = subprocess.run(
                ['nmap', '-sn', network, '--host-timeout', '10'],
                encoding='utf-8',
                stdout=subprocess.PIPE
            ).stdout
            for ip in re.findall(ip_regex, stdout):
                if not ip.endswith('.1'):
                    ips.append(ip)

    with paramiko.SSHClient() as client:
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        passwd = getpass('Enter your main SSH passwords to collect hostnames: ')
        to_csv = list()
        print('Collecting hostnames...')
        for t, ip in enumerate(ips):
            progress_line(t + 1, len(ips))
            try:
                client.connect(hostname=ip, username='root', password=passwd)
                _, out, _ = client.exec_command('hostname')
                to_csv.append({'ip': ip, 'hostname': out.read().decode("utf-8").strip()})
            except paramiko.ssh_exception.AuthenticationException:
                to_csv.append({'ip': ip, 'hostname': 'unreachable'})

    with open('output.csv', 'w') as file:
        wr = csv.DictWriter(file, fieldnames=['ip', 'hostname'])
        wr.writeheader()
        wr.writerows(to_csv)
