#!/usr/bin/env python3
# Author: Daniil Fedorov
# License: BSD
import argparse
from libs.tslogging import TsLogging


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script is used ...')
    parser.add_argument('--dry-run', action='store_true', help='dry run, do not process')
    TsLogging.add_args(parser)
    args = parser.parse_args()
    log = TsLogging(args)
